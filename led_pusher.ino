#include <PubSubClient.h>
#include <ESP8266WiFi.h>
#include <WiFiClient.h>

const char* ssid = "wlanssid";
const char* password = "xxxxxxxxxxxxxxxxx";
const int led = LED_BUILTIN;

 
WiFiClient wifiClient;
PubSubClient mqtt;

long lastMsg = 0;
char msg[50];
int value = 0;

void setup(void) {
  Serial.begin(115200);

  WiFi.begin(ssid, password);

  //Wait for connection no errorcatching
  while (WiFi.status() != WL_CONNECTED) {
    delay(50);
    Serial.print(".");
  }

  Serial.print("Connected to ");
  Serial.println(ssid);
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
 
  mqtt.setClient(wifiClient);
  mqtt.setServer("192.168.100.66", 1883);

  //mqtt.subscribe("blink-led-7533");
  // client is now configured for use

  mqtt.connect("esptest");
  mqtt.publish("blink-led-1", "high");
  delay(5000);
  mqtt.publish("blink-led-1", "low");
  shutdown();
}

void loop(void) {

}

void shutdown() {
  Serial.println("Shutting down.");
  Serial.println("Going to sleep.");
  ESP.deepSleep(0);
  Serial.println("Sleep failed.");
}
