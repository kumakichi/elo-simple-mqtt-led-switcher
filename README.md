# elo-simple-mqtt-led-switcher

For the family of Wemos / Linode Modules. Performs simple WLAN auth and pushes command or receives command to switch an LED on/off. Can be modified easily to do what you like.
Very basic code with lots of debug options and more or less intended for beginners, who are just starting with the ESPs/Wemos/Linodes.

Requires certain libs, check code please.

