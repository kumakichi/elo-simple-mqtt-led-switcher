#include <PubSubClient.h>
#include <ESP8266WiFi.h>
#include <WiFiClient.h>

const char* ssid = "wlanssid";
const char* password = "xxxxxxxxxxxxx";
const int led = LED_BUILTIN;
const int green = D2;
const int red = D3;

WiFiClient wifiClient;
PubSubClient mqtt;

long lastMsg = 0;
char msg[50];
int value = 0;

void callback(char* topic, byte* payload, unsigned int length) {
switch ((char)payload[0]) {
    case "greenon":
      digitalWrite(green, HIGH);
      break;
    case "greenoff":
      digitalWrite(green, LOW);
      break;
    case "redon":
      digitalWrite(green, HIGH);
      break;
    case "redoff":
      digitalWrite(green, LOW);
      break;
    default:
      // if nothing else matches, do the default
      // default is optional
    break;
  }  
}

void setup(void){
  pinMode(green, OUTPUT);
  pinMode(red, OUTPUT);
  pinMode(led, OUTPUT);

  digitalWrite(green, LOW);
  digitalWrite(red, LOW);
  
  Serial.begin(115200);

  WiFi.begin(ssid, password);

  // Wait for connection
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.print("Connected to ");
  Serial.println(ssid);
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());

    mqtt.setClient(wifiClient);
    mqtt.setServer("192.168.100.66",1883);
    mqtt.connect("esptest");
    mqtt.subscribe("blink-led-1");
    mqtt.setCallback(callback); 
    // client is now configured for use
}
 

void loop(void){
  mqtt.loop();
}
